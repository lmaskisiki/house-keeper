module profile.management {
    requires spring.boot.starter;
    requires spring.context;
    requires reactor.core;
    requires spring.data.mongodb;
    requires spring.beans;
    requires spring.webflux;
    requires mongodb.driver.reactivestreams;
    requires spring.boot.autoconfigure;
    requires spring.cloud.netflix.eureka.client;
    requires spring.boot;
    requires spring.web;
    requires spring.core;
    requires ecompare.shared;
    exports com.ecompare.userprofile;
}