package com.ecompare.userprofile.controllers;

import com.ecompare.userprofile.services.UserService;
import ecompare.shared.models.User;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/user-profile")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public Mono<User> createUser(@RequestBody User user) {
        Assert.notNull(user, "Invalid user details");
        Assert.hasLength(user.getEmailAddress(), "Email address is required");
        Assert.hasLength(user.getEmailAddress(), "Cell number is required");
        Assert.hasLength(user.getIdNumber(), "Id number is required");
        return userService.save(user);
    }

    @PostMapping("/find/username")
    public Mono<User> findByUsername(@RequestBody String username) {
        System.out.println("Here is the username provided ::" + username);
        return userService.findByUsername(username);
    }

    @GetMapping("/all")
    public Flux<User> getUsers() {
        return userService.findAll();
    }

    @PostMapping("/update/password/{userId}")
    public Mono<User> updateUserPasssword(@RequestBody String newPassword, @PathVariable String userId) {
          return userService.updateUserPassword(newPassword, userId);
     }

}
