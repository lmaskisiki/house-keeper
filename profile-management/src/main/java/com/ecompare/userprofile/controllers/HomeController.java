package com.ecompare.userprofile.controllers;

import com.ecompare.userprofile.UserRepository;
import ecompare.shared.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.time.LocalDate;
import java.util.UUID;

@RestController
@RequestMapping("/api/user-profile")
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public Flux<User> users() {
        return userRepository.findAll().map(HomeController::clearPassword);
    }

    private static User clearPassword(User a) {
        var newUser = a;
        newUser.setPassword(null);
        return newUser;
    }
}
