package com.ecompare.userprofile.services;


import ecompare.shared.models.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
    Mono<User> save(User user);

    Flux<User> findAll();

    Mono<User> findByUsername(String username);

    Mono<User> updateUserPassword(String newPassword, String userId);
}

