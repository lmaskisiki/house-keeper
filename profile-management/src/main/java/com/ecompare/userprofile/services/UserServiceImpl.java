package com.ecompare.userprofile.services;

import com.ecompare.userprofile.UserRepository;
import ecompare.shared.models.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public Mono<User> save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Flux<User> findAll() {
        return userRepository.findAll().map(UserServiceImpl::clearPassword);
    }

    @Override
    public Mono<User> findByUsername(String username) {
        return userRepository.findByEmailAddress(username);
    }

    @Override
    public Mono<User> updateUserPassword(String newPassword, String userId) {
        return userRepository.findById(UUID.fromString(userId))
                .flatMap(user -> {
                    user.setPassword(newPassword);
                    return userRepository.save(user);
                });
    }

    private static User clearPassword(User user) {
        user.setPassword(null);
        return user;
    }
}
