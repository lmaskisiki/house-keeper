package ecompare.shared.models;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.UUID;

public class Expense {

  @Id
  private UUID id = UUID.randomUUID();
  private String name;
  private String  description;
  private String category;
  private double amount;
  private LocalDate startDate;
  private LocalDate endDate;


  public Expense(String name, String description, String category, double amount, LocalDate startDate, LocalDate endDate) {
    this.name = name;
    this.description = description;
    this.category = category;
    this.amount = amount;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public Expense() {
   }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  @Override
  public String toString() {
    return "Expense{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", category='" + category + '\'' +
      ", amount=" + amount +
      ", startDate=" + startDate +
      ", endDate=" + endDate +
      '}';
  }
}
