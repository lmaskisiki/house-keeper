package ecompare.shared.models;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.UUID;


public class Note {

  @Id
  private UUID id;
  private String name;
  private LocalDate date= LocalDate.now();
  private String text;

  public Note(String text) {
    this.id = UUID.randomUUID();
    this.text = text;
  }

  public Note() {
    this.id = UUID.randomUUID();
    this.date = LocalDate.now();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
