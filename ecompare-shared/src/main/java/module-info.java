module ecompare.shared {
    requires spring.data.commons;
    requires com.fasterxml.jackson.databind;
    exports ecompare.shared.models;
}