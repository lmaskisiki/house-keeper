module rest.client {
  requires ecompare.shared;
  requires spring.core;
  requires spring.web;
  requires reactor.core;
  requires spring.cloud.openfeign.core;
  requires spring.cloud.netflix.ribbon;
  requires spring.boot.autoconfigure;
  requires springfox.swagger2;
  requires spring.boot;
  requires spring.context;
  requires springfox.spring.web;
  requires springfox.core;
  requires spring.webflux;
}
