package com.ecompare.restclient.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.config.WebFluxConfigurerComposite;

@Configuration
public class CorsConfig {

  @Bean
  public WebFluxConfigurer corsConfigurer() {
    return new WebFluxConfigurerComposite() {

      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
          .allowedMethods("*")
          .allowedOrigins("http://localhost:4200",
            "http://localhost:8180",
            "http://localhost","http://localhost:80","http://web.masikisiki.co.za","http://159.89.149.177","159.89.149.177");
      }
    };
  }
}
