package com.ecompare.restclient.endpoints;

import ecompare.shared.models.User;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@FeignClient(name = "profile-management")
@RibbonClient(name = "profile-management")
public interface UserProfile {

    @PostMapping("/api/user-profile/create")
    public User createUser(@RequestBody User user);

    @GetMapping("/api/user-profile/all")
    public List<User> getUsers();

    @PostMapping("/api/user-profile/find/username")
    public User findByUsername(String username);

    @PostMapping("/api/user-profile/update/password/{userId}")
    public User updateUserPassword(@RequestBody String newPassword, @PathVariable String userId);

 }
