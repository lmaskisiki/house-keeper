package com.ecompare.restclient.endpoints;


import ecompare.shared.models.Expense;
import ecompare.shared.models.Note;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@FeignClient(name = "expenses")
@RibbonClient(name = "expenses")
public interface Expenses {
  @PostMapping("/api/expense/create")
  Expense createExpense(@RequestBody Expense expense);

  @DeleteMapping("/api/expense/delete/{id}")
  Object deleteExpenseById(@PathVariable String id);

  @PutMapping("/api/expense/update")
  Expense updateExpense(@RequestBody Expense expense);

  @GetMapping("/api/expense/all/")
  @ResponseBody
  List<Expense> getAllExpenses();

  //  NOTES
  @PostMapping("/api/note/create")
  Note createNote(@RequestBody Note note);

  @GetMapping("/api/note/all")
  List<Note> getNotes();

  @DeleteMapping("/api/note/delete/{id}")
  Object deleteNote(@PathVariable String id);
}

