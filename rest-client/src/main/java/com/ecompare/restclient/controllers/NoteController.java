package com.ecompare.restclient.controllers;


import ecompare.shared.models.Expense;
import com.ecompare.restclient.endpoints.Expenses;
import ecompare.shared.models.Note;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@RestController
@RequestMapping("/api/client/note")
public class NoteController {

  private final Expenses expenses;

  public NoteController(Expenses expenses) {
    this.expenses = expenses;
  }

  @GetMapping("/all")
  public Flux<Note> getNotes() {
    return Flux.fromIterable(expenses.getNotes());
  }

  @PostMapping(value = "/create", consumes = {"text/plain", "application/json"})
  public Mono<Note> createNote(@RequestBody Note note) {
    return Mono.just(expenses.createNote(note));
  }

  @DeleteMapping("/delete/{id}")
  public Mono<ServerResponse> deleteNote(@PathVariable String id) {
    expenses.deleteNote(id);
    return ok().body(BodyInserters.empty());
  }
}
