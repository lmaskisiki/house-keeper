package com.ecompare.restclient.controllers;

import com.ecompare.restclient.endpoints.UserProfile;
import ecompare.shared.models.User;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/client/user-profile")
@CrossOrigin(origins = {"http://localhost","http://localhost:4200","http://localhost:80","http://web.masikisiki.co.za","http://159.89.149.177"}, methods = {RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.HEAD, RequestMethod.GET, RequestMethod.PUT, RequestMethod.TRACE})
public class
UserProfileController {

    private final UserProfile userController;

    public UserProfileController(UserProfile userController) {
        this.userController = userController;
    }

    @PostMapping("/create")
    public Mono<User> createUser(@RequestBody User user) {
         return Mono.just(userController.createUser(user));
    }

    @PostMapping("/find/username")
    public Mono<User> findByUsername(@RequestBody String username) {
         return Mono.just(userController.findByUsername(username));
    }

    @PostMapping("/update/password/{userId}")
    public Mono<User> updateUserPasssword(@RequestBody String newPassword, @PathVariable String userId) {
        return Mono.just( userController.updateUserPassword(newPassword, userId));
    }

    @GetMapping("/all")
    public Flux<User> getAll() {
        return Flux.fromIterable(userController.getUsers());
    }
}
