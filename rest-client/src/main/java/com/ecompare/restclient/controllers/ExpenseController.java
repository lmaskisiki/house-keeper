package com.ecompare.restclient.controllers;

import com.ecompare.restclient.endpoints.Expenses;
import ecompare.shared.models.Expense;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@RestController
@RequestMapping("/api/client/expenses")
public class ExpenseController {


  private final Expenses expenses;

  public ExpenseController(Expenses expenses) {
    this.expenses = expenses;
  }

  @GetMapping("/all")
  public Flux<Expense> getExpenses() {
    return Flux.fromIterable(expenses.getAllExpenses());
  }

  @PostMapping("/create")
  public Mono<Expense> createExpense(@RequestBody Expense expense) {
    return Mono.just(expenses.createExpense(expense));
  }

  @PutMapping("/update")
  public Mono<Expense> updateExpense(@RequestBody Expense expense) {
    return Mono.just(expenses.updateExpense(expense));
  }

  @DeleteMapping("/delete/{id}")
  public Mono<ServerResponse> deleteExpense(@PathVariable String id) {
    expenses.deleteExpenseById(id);
    return ok().body(BodyInserters.empty());
  }
}
