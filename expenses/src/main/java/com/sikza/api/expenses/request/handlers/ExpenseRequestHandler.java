package com.sikza.api.expenses.request.handlers;

import ecompare.shared.models.Expense;
import com.sikza.api.expenses.repositories.ExpenseRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Component
public class ExpenseRequestHandler {

  private final ExpenseRepository expenseRepository;

  public ExpenseRequestHandler(ExpenseRepository expenseRepository) {
    this.expenseRepository = expenseRepository;
  }


  public Mono<ServerResponse> createExpense(ServerRequest expenseRequest) {
    var body = expenseRequest.bodyToMono(Expense.class);
    return ServerResponse.ok()
      .body(BodyInserters.fromPublisher(body
        .flatMap(b -> expenseRepository.save(b)), Expense.class));
  }

  public Mono<ServerResponse> getAllExpenses(ServerRequest serverRequest) {
    return ServerResponse.ok()
      .contentType(MediaType.APPLICATION_JSON)
      .body(BodyInserters.fromPublisher(expenseRepository.findAllActive(LocalDate.now()), Expense.class));
  }


  public Mono<ServerResponse> deleteById(ServerRequest serverRequest) {
    var id = serverRequest.pathVariable("id");
    Assert.hasLength(id, "expense id is required to delete");
    var delete = expenseRepository.deleteById(UUID.fromString(id));
    return ServerResponse.ok().body(BodyInserters.fromPublisher(delete, void.class));
  }
}
