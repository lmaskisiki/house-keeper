package com.sikza.api.expenses.repositories;

import ecompare.shared.models.Expense;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.UUID;

@Repository
public interface ExpenseRepository extends ReactiveMongoRepository<Expense, UUID> {
  @Query("{'endDate':{$gt: ?0}}")
  Flux<Expense> findAllActive(LocalDate date);
}
