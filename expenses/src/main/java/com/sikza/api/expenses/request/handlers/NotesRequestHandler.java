package com.sikza.api.expenses.request.handlers;

import com.sikza.api.expenses.repositories.NotesRepository;
import ecompare.shared.models.Expense;
import ecompare.shared.models.Note;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
public class NotesRequestHandler {

  private final NotesRepository notesRepository;

  public NotesRequestHandler(NotesRepository notesRepository) {
    this.notesRepository = notesRepository;
  }

  public Mono<ServerResponse> createNote(ServerRequest expenseRequest) {
    var body = expenseRequest.bodyToMono(Note.class);
    return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
      .body(BodyInserters.fromPublisher(body
        .flatMap(b -> notesRepository.save(b)), Note.class));
  }

  public Mono<ServerResponse> getNotes(ServerRequest serverRequest) {
    return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromPublisher(notesRepository.findAll(), Note.class));
  }

  public Mono<ServerResponse> deleteNote(ServerRequest serverRequest) {
    var noteId = serverRequest.pathVariable("id");
    return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromPublisher(notesRepository.deleteById(UUID.fromString(noteId)), Void.class));
  }
}
