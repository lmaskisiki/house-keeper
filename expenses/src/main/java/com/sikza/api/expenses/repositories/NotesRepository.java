package com.sikza.api.expenses.repositories;

import ecompare.shared.models.Note;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NotesRepository extends ReactiveMongoRepository<Note, UUID> {
}
