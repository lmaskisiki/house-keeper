package com.sikza.api.expenses.routes;

import com.sikza.api.expenses.request.handlers.ExpenseRequestHandler;
import com.sikza.api.expenses.request.handlers.NotesRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.http.MediaType;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;

@Configuration
public class ExpenseRouting {

  @Autowired
  private ExpenseRequestHandler expenseRequestHandler;

  @Bean
  public RouterFunction<ServerResponse> root(ExpenseRequestHandler handler) {
    return RouterFunctions.route()
      .POST("/api/expense/create",
        accept(APPLICATION_JSON)
          .and(contentType(APPLICATION_JSON))
        , handler::createExpense)
      .PUT("/api/expense/update", accept(APPLICATION_JSON), handler::createExpense)
      .GET("/api/expense/all", accept(APPLICATION_JSON), handler::getAllExpenses)
      .DELETE("/api/expense/delete/{id}", accept(APPLICATION_JSON), handler::deleteById)
      .build();
  }

  @Bean
  public RouterFunction<ServerResponse> notesRoutes(NotesRequestHandler handler) {
    return RouterFunctions.route()
      .POST("/api/note/create", accept(APPLICATION_JSON, TEXT_PLAIN), handler::createNote)
      .GET("/api/note/all", accept(APPLICATION_JSON), handler::getNotes)
      .DELETE("/api/note/delete/{id}", accept(APPLICATION_JSON), handler::deleteNote)
      .build();
  }

}
