module sikza.expenses {
  requires ecompare.shared;
  requires spring.boot;
  requires spring.boot.autoconfigure;
  requires spring.context;
  requires spring.webflux;
  requires spring.beans;
  requires spring.web;
  requires spring.core;
  requires reactor.core;
  requires spring.data.mongodb;
}
