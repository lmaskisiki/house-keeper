import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatGridListModule,
  MatTabsModule,
  MatSidenavModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatListModule,
  MatFormField,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatCheckboxModule,
  MatDatepicker, MatDatepickerModule,
  MatDialogModule,
  MatNativeDateModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { Route, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from './base-layout/base-layout.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SingInComponent } from './sing-in/sing-in.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ItemComponent } from './item/item.component';
import { ItemService } from './_services/item.service';
import { BrowseComponent } from './browse/browse.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// import { library } from '@fortawesome/free-regular-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fas,
  faSignInAlt,
  faSignOutAlt,
  faShoppingCart
} from '@fortawesome/free-solid-svg-icons';
import { ItemModalComponent } from './item-modal/item-modal.component';
import { CartService } from './_services/cart.service';
import { CartComponent } from './cart/cart.component';
import { PasswordSettingsComponent } from './password-settings/password-settings.component';
import { RetailersService } from './_services/retailers.service';
import { UserProfileService } from './_services/user-profile.service';

library.add(fas, faSignInAlt, faShoppingCart, faSignOutAlt);

const routes: Route[] = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'browse', component: BrowseComponent },
      { path: 'home', component: ItemComponent },
      { path: 'login', component: SingInComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'password/user/:id', component: PasswordSettingsComponent },
      { path: 'cart', component: CartComponent }
    ]
  },
  { path: 'login', component: SingInComponent }];
@NgModule({
  declarations: [
    AppComponent,
    BaseLayoutComponent,
    SingInComponent,
    RegisterComponent,
    ItemComponent,
    BrowseComponent,
    ItemModalComponent,
    CartComponent,
    PasswordSettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatOptionModule,
    MatSelectModule,
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatListModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSidenavModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ItemService, CartService, UserProfileService, RetailersService],
  bootstrap: [AppComponent],
  entryComponents: [ItemModalComponent]
})
export class AppModule { }
