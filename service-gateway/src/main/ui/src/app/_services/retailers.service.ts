import { Injectable } from '@angular/core';
import { of, Observable, Subject } from 'rxjs';
import { Retailer } from '../_models/retailer';

@Injectable({
  providedIn: 'root'
})
export class RetailersService {

  constructor() { }
  retailers = new Subject<Retailer>();
  getRetailers(): void {
    let x = [
      new Retailer('1', 'Spar'),
      new Retailer('2', 'Shoprite'),
      new Retailer('3', 'Woolworths'),
      new Retailer('4', 'Food Lovers'),
      new Retailer('5', 'Pinck n Pay'),
      new Retailer('6', 'Cambridge')
    ];
    x.forEach(a => this.retailers.next(a));
  }

  addRetailer(value: Retailer): void {
     this.retailers.next(value);
  }
}
