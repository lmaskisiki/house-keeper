import { Injectable } from '@angular/core';
import { Item } from '../_models/item';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor() { }

  itemsSubscription = new Subject<Item[]>();
  private items: Item[] =[];

  addToCart(item: Item){
    this.itemsSubscription.next([item]);
    this.items.push(item);
  }

  getCartItems(){
    return this.items;
  }
}
