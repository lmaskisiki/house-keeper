import { Injectable } from '@angular/core';
import { UserProfile } from '../_models/user-profile';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { Observable, Subscription, Subject, BehaviorSubject, GroupedObservable } from 'rxjs';
import { Expense } from '../_models/expense';
import { environment } from "../../environments/environment";
import { map, groupBy } from 'rxjs/operators';
import { Transaction } from '../_models/transaction';
@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  private transactionEvents = new Subject<Transaction>();
  activeUserId: String;
  private expenses: Expense[] = [];
  expenseSuject: BehaviorSubject<Expense[]> = new BehaviorSubject<Expense[]>(this.expenses);
  constructor(private router: Router, private http: HttpClient) { }

  createAccount(userProfile: UserProfile): any {
    const accounts = this.getUserAccounts();
    userProfile.id = `${accounts.length + 1}`;
    accounts.push(userProfile);
    this.saveToLocalStore(accounts);
    const body = {
      'firstName': userProfile.firstName,
      'lastName': userProfile.lastName,
      'idNumber': userProfile.idNumber,
      'emailAddress': userProfile.emailAddress,
      'cellNumber': userProfile.cellNumber,
      'password': userProfile.password,
      'title': userProfile.title,
    };
    this.http.post<UserProfile>('/api/client/user-profile/create', body)
      .subscribe(a => {
        this.activeUserId = a.id;
        this.router.navigate(['/user/password']);
      }, e => console.log(e));
  }

  saveToLocalStore(accounts: UserProfile[]) {
    localStorage.setItem('accounts', JSON.stringify(accounts));
  }

  setUserPassword(userId: String, password: String) {
    const users = this.getUserAccounts();
    const user = users.find(u => u.id === userId);
    if (user) {
      user.password = password;
      users.splice(users.indexOf(user), 1);
      users.push(user);
    }

    this.saveToLocalStore(users);
    this.http.post(`/gateway/web/password/${this.activeUserId}`, password)
      .subscribe(
        () => this.router.navigate(['/login']),
        err => alert('Could not update password\n' + JSON.stringify(err)));
  }

  getUserAccounts(): UserProfile[] {
    const accountsStr = localStorage.getItem('accounts');
    let accounts: UserProfile[] = [];
    if (accountsStr) {
      accounts = JSON.parse(accountsStr) as UserProfile[];
    }
    return accounts;
  }

  login(username: string, password: string) {
    const accounts = this.getUserAccounts();
    const currentUser = accounts.filter(a => a.emailAddress === username && a.password === password);
    if (currentUser.length > 0) {
      this.router.navigate(['/browse']);
    }
    return 'invalid username or password';
  }

  getExpenses() {
    if (this.expenses.length === 0) {
      this.fetchExpenses();
    }

    return this.expenseSuject;
  }

  saveExpense(body: Expense): Subscription {
    delete body.id;
    return this.http.post<Expense>(`${environment.baseUrl}/api/client/expenses/create`, body)
      .subscribe(newExpense => {
        this.expenses.push(newExpense);
      });
  }

  deleteExpense(expenseId: String): Observable<any> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.delete(`${environment.baseUrl}/api/client/expenses/delete/${expenseId}`, { headers: httpHeaders });
  }

  updateExpense(body: Expense): Subscription {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put<Expense>(`${environment.baseUrl}/api/client/expenses/update/`, body, { headers: httpHeaders }).subscribe(updated => {
    });
  }

  public getFinTransactions(): Observable<GroupedObservable<string, Transaction>> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'text/event-stream',
      'Accept': 'text/event-stream'
    });
    
    // var evtSource = new EventSource(`${environment.baseUrl}/api/fin/transactions/`, { withCredentials: false });
    // evtSource.addEventListener('message', (event: MessageEvent) => {
    //   if (event && event.data) {
    //     let data = JSON.parse(event.data);
    //     this.transactionEvents.next(data as Transaction)
    //   }
    // })
    this.http.get<Transaction[]>(`${environment.baseUrl}/api/fin/transactions/`).subscribe(trans =>trans.forEach(t=>this.transactionEvents.next(t)))
    return this.transactionEvents.asObservable().pipe(groupBy(a => a.category));
  }

  public upload(data) {
    let uploadURL = `${environment.baseUrl}/api/fin/upload/`;
    return this.http.post<any>(uploadURL, data, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
  }

  private fetchExpenses(): void {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.http.get<Expense[]>(`${environment.baseUrl}/api/client/expenses/all`,{headers: httpHeaders}).subscribe(expns => this.expenseSuject.next(expns));
  }
}


