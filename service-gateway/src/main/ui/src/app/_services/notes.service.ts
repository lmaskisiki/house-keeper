import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private http: HttpClient) { }

  public getNotes(): Observable<{ id: string, text: string, name:string }[]> {
    return this.http.get<{ id: string, text: string, name:string }[]>(`${environment.baseUrl}/api/client/note/all`);
  }

  public saveNotes(note: {text:string, name:string}): Observable<any> {
    return this.http.post(`${environment.baseUrl}/api/client/note/create`, note);
  }

}
