import { Injectable } from "@angular/core";
import { Item } from "../_models/item";
import { Vendor } from "../_models/vendor";

@Injectable({
  providedIn: "root"
})
export class ItemService {
  constructor() {}

  getItems() : Item[] {
    const items = [
      new Item(
        "Olive Oil",
        "1l Green Olive Oil",
        22.4,
        "1lt",
        "../../assets/olive_oil.png",
        "Cooking",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Brown Bread",
        "1 brown bread loaf",
        8.4,
        "1 loaf",
        "../../assets/bread.png",
        "Bakery",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Milk",
        "1kg Low Fat Milk",
        12.4,
        "1lt",
        "../../assets/milk.png",
        "Breakfast",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Juice",
        "1l Orange Juice",
        17.4,
        "1lt",
        "../../assets/juice.png",
        "Drinks",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Juice",
        "1l Orange Juice",
        17.4,
        "1lt",
        "../../assets/juice_1.png",
        "Drinks",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Juice",
        "1l Orange Juice",
        17.4,
        "1lt",
        "../../assets/juice_1.png",
        "Drinks",
        new Vendor("Woolworths")
      ),
      new Item(
        "Juice",
        "1l Orange Juice",
        17.4,
        "1lt",
        "../../assets/juice_1.png",
        "Drinks",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Salt",
        "500g Salt",
        8.4,
        "500g",
        "../../assets/salt_PNG22361.png",
        "Cooking",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Soursage",
        "Sousage",
        37.4,
        "1kg",
        "../../assets/sausage_1.png",
        "Buchery",
        new Vendor("Woolworths")
      ),
      new Item(
        "Soursage",
        "Sousage",
        47.4,
        "1kg",
        "../../assets/sausage_PNG5195.png",
        "Buchery",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Meat",
        "Beef Meet",
        25.4,
         "1kg",
        "../../assets/meat_1.png",
        "Buchery",
        new Vendor("Woolworths")
      ),
      new Item(
        "Meat",
        "Beef Meet",
        27.4,
        "1kg",
        "../../assets/meat.png",
        "Buchery",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Eggs",
        "16 Eggs",
        31.4,
        "16",
        "../../assets/egg_PNG40811.png",
        "Breakfast",
        new Vendor("Woolworths")
      ),
      new Item(
        "Eggs",
        "16 Nice Eggs",
        42.4,
        "16s",
        "../../assets/egg_PNG40806.png",
        "Breakfast",
        new Vendor("Woolworths")
      ),
      new Item(
        "Eggs",
        "16s Eggs",
        34.4,
        "16",
        "../../assets/egg_PNG40807.png",
        "Breakfast",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Juice",
        "1l Orange Juice",
        17.4,
        "1lt",
        "../../assets/juice_1.png",
        "Drinks",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Book",
        "2Q Book",
        11.84,
        "2Q",
        "../../assets/book_PNG51109.png",
        "School",
        new Vendor("Kwik Spar")
      ),
       new Item(
        "Book",
        "2Q Book",
        13.84,
        "2Q",
        "../../assets/book_PNG51112.png",
        "School",
        new Vendor("Kwik Spar")
      ),
      new Item(
        "Book",
        "4Q Book",
        12.4,
        "2Q",
        "../../assets/book_PNG51108.png",
        "School",
        new Vendor("Woolworths")
      ),
      new Item(
        "Salt",
        "500g Salt",
        7.40,
        "2Q",
        "../../assets/salt_PNG22367.png",
        "Cooking",
        new Vendor("Woolworths")
      ),
      new Item(
        "Salt",
        "500g Salt",
        9.40,
        "2Q",
        "../../assets/salt_PNG22362.png",
        "Cooking",
        new Vendor("Spar")
      )
    ];

    return items;
  }
}
