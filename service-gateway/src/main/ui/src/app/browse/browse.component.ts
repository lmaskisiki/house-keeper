import { Component, OnInit } from "@angular/core";
import { ItemService } from "../_services/item.service";
import { Item } from "../_models/item";

@Component({
  selector: "app-browse",
  templateUrl: "./browse.component.html",
  styleUrls: ["./browse.component.css"]
})
export class BrowseComponent implements OnInit {
  items: Item[] = [];
  retailers: Set<string>;
  categories: Set<string>;
  tempItems: Item[];
  selectedCategory: string;
  selectedRetailer: string;
  constructor(private itemService: ItemService) {}

  ngOnInit() {
    this.items = this.itemService.getItems();
    this.tempItems = this.items;
    this.getCategories();
    this.getRetailers();
  }

  getCategories(): any {
    this.categories = new Set(this.items.map(a => a.category));
  }

  getRetailers(): any {
    this.retailers = new Set(this.items.map(a => a.vendor.name));
  }

  byCategory() {
    this.items = this.tempItems.filter(
      a => a.category === this.selectedCategory
    );
  }

  byRetailer() {
    this.items = this.tempItems.filter(
      a => a.vendor.name === this.selectedRetailer
    );
  }
}
