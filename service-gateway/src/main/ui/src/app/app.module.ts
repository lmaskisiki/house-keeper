import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatGridListModule,
  MatTabsModule,
  MatSidenavModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatListModule,
  MatFormField,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatRadioModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDatepicker,
  MatDatepickerModule,
  MatNativeDateModule,
  MAT_DIALOG_DATA,
  MatTableModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { Route, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from './base-layout/base-layout.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SingInComponent } from './sing-in/sing-in.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ItemComponent } from './item/item.component';
import { ItemService } from './_services/item.service';
import { BrowseComponent } from './browse/browse.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// import { library } from '@fortawesome/free-regular-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fas,
  faSignInAlt,
  faSignOutAlt,
  faShoppingCart
} from '@fortawesome/free-solid-svg-icons';
import { ItemModalComponent } from './item-modal/item-modal.component';
import { CartService } from './_services/cart.service';
import { CartComponent } from './cart/cart.component';
import { PasswordSettingsComponent } from './password-settings/password-settings.component';
import { RetailersService } from './_services/retailers.service';
import { UserProfileService } from './_services/user-profile.service';
import { RetailerComponent } from './retailer/retailer.component';
import { NewRetailerComponent } from './new-retailer/new-retailer.component';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { ExpensesComponent } from './expenses/expenses.component';
import { ExpenseFormComponent } from './expenses/expense-form/expense-form.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FinTransactionsComponent } from './fin-transactions/fin-transactions.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { ChartsModule } from "ng2-charts";
import { ChartsComponent } from './user-dashboard/charts/charts.component';
import { NotesComponent } from './notes/notes.component';
import { InputBoxDialog } from "./notes/input-box-dialog";
import { NotesService } from './_services/notes.service';
import { ChartCardComponent } from './user-dashboard/chart-card/chart-card.component';
import { PillContainerComponent } from './common/pill-container/pill-container.component';
library.add(fas, faSignInAlt, faShoppingCart, faSignOutAlt);

const routes: Route[] = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'browse', component: BrowseComponent },
      { path: 'retailers', component: RetailerComponent },
      { path: 'home', component: ItemComponent },
      { path: 'login', component: SingInComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'notes', component:NotesComponent },
      { path: 'user/password', component: PasswordSettingsComponent },
      { path: 'cart', component: CartComponent },
      {
        path: 'dashboard', component: UserDashboardComponent, children: [
          { path: '**', component: ChartsComponent }
        ]
      },
      { path: 'expenses', component: ExpensesComponent },
      { path: 'transactions', component: FinTransactionsComponent },
      { path: 'expenses/create', component: ExpenseFormComponent },
      { path: 'expenses/update/:id', component: ExpenseFormComponent }
    ]
  },
  { path: 'login', component: SingInComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    BaseLayoutComponent,
    SingInComponent,
    RegisterComponent,
    ItemComponent,
    BrowseComponent,
    ItemModalComponent,
    CartComponent,
    PasswordSettingsComponent,
    RetailerComponent,
    NewRetailerComponent,
    ExpensesComponent,
    ExpenseFormComponent,
    FinTransactionsComponent,
    UserDashboardComponent,
    ChartsComponent,
    NotesComponent,
    ChartCardComponent,
    InputBoxDialog,
    PillContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatListModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSidenavModule,
    MatDialogModule,
    MatTableModule,
    MatListModule,
    MatNativeDateModule,
    MatTableModule,
    MatDatepickerModule,
    HttpClientModule,
    ChartsModule,
    RouterModule.forRoot(routes),
    AgmCoreModule.forRoot({
      apiKey: 'sss'
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ItemService, CartService, UserProfileService, RetailersService, NotesService],
  bootstrap: [AppComponent],
  entryComponents: [ItemModalComponent, NewRetailerComponent, ChartCardComponent, InputBoxDialog]
})
export class AppModule { }
