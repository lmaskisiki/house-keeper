import { Component, OnInit } from '@angular/core';
import { FormBuilder, Form, FormGroup, Validators } from '@angular/forms';
import { UserProfileService } from '../_services/user-profile.service';
import { RetailersService } from '../_services/retailers.service';
import { Retailer } from '../_models/retailer';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  submitted = false;
  retailers = new Array<Retailer>();
  constructor(
    private formBuilder: FormBuilder,
    private retailerService: RetailersService,
    private userProfileService: UserProfileService
  ) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      title: ['', Validators.required],
      idNumber: ['', Validators.required],
      cellNumber: ['', Validators.required],
      emailAddress: ['', Validators.required],
      role: ['', Validators.required],
      storeName: [''],
      companyName: [''],

      address: this.formBuilder.group({
        line1: ['', Validators.required],
        line2: [''],
        suburb: ['', Validators.required],
        city: ['', Validators.required],
        postalCode: ['', Validators.required],
        province: ['', Validators.required]
      })
    });

    this.registrationForm.get('role').valueChanges.subscribe(value => {
      if (value === 'Shopper') {
        this.registrationForm.get('storeName').setValidators([]);
      }
      if (value === 'Store') {
        this.registrationForm.get('storeName').setValidators(Validators.required);
      }
    });

    this.retailerService.retailers.subscribe(val => this.retailers.push(val));
    this.retailerService.getRetailers();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.registrationForm.controls[controlName].hasError(errorName);
  }

  public register() {
    this.submitted = true;
    if (this.registrationForm.invalid) {
      return;
    }

    this.userProfileService.createAccount(this.registrationForm.value);
  }
}
