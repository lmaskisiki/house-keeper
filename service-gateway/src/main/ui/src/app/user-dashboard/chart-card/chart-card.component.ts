import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart-card',
  templateUrl: './chart-card.component.html',
  styleUrls: ['./chart-card.component.css']
})
export class ChartCardComponent implements OnInit {
  data: {
    chartType: string;
    expenseChartData: {
      data: number[];
      backgroundColor: string[];
      label: string;
    }[];
    expenseChartLabels: string[];
    expenseChartOptions: { };
  };

  constructor() {
  }

  ngOnInit() {
  }

}
