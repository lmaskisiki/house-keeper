import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { UserProfileService } from '../../_services/user-profile.service';
import { ChartCardComponent } from '../chart-card/chart-card.component';
import { Transaction } from '../../_models/transaction';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  expenseChartLabels: string[] = [];
  expenseChartOptions: {};
  expenseChartData: { data: number[]; backgroundColor: string[], label: string; }[];
  subChartData: { chartType: string, data: any[], labels: string[], options: {} };
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  groups: { [key: string]: number } = {}
  chartType = "bar";
  allFinTransactions: Transaction[] = [];

  monthFilter: { month: number, year: number };
  transactionMonths: { [month: string]: { month: number, year: number } } = {};
  @ViewChild('chartsSection', { read: ViewContainerRef }) chartsSection;
  filtered: Transaction[];
  constructor(private profileService: UserProfileService,

    private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.getTransactions();
  }

  monthFilterChanged() {
    this.filtered = this.allFinTransactions.filter(a =>
      new Date(a.date).getMonth() === this.monthFilter.month && new Date(a.date).getFullYear() === this.monthFilter.year);

    const labels = new Set(this.filtered.map(a => a.category));
    const data = [];
    labels.forEach(label => {
      const val = this.filtered.filter(a => a.category === label).map(a => a.cost).reduce((s, v) => s += v);
      data.push(val);
    });

    this.expenseChartLabels = Array.from(labels);
    this.expenseChartData[0].data = data;
    this.expenseChartData[0].backgroundColor = this.getRandomColors(this.expenseChartLabels.length);
  }

  private getTransactions() {
    this.profileService.getFinTransactions().subscribe(group => {
      group.subscribe(val => {
        if (!this.groups[group.key]) {
          this.groups[group.key] = 0;
        }
        this.groups[group.key] += val.cost;
        val.date = new Date(val.date)
        this.transactionMonths[`${this.months[val.date.getMonth()]}-${val.date.getFullYear()}`] = {
          month: val.date.getMonth(),
          year: val.date.getFullYear()
        };
        this.allFinTransactions.push(val);
        this.createChartsData()
      });
    });
  }

  getRandomColors(size: number): string[] {
    const shuffled = COLOR_OPTIONS.sort(function () { return 0.5 - Math.random() });
    let colors = [];
    for (let index = 0; index < size; index++) {
      colors.push(shuffled[index]);
    }
    return colors;
  }

  get transMonths(): string[] {
    return Object.keys(this.transactionMonths);
  }
  createChartsData() {
    this.expenseChartLabels = Object.keys(this.groups);
    const dataSet1 = [];
    this.expenseChartLabels.forEach(label => dataSet1.push(this.groups[label]));
    this.expenseChartData = [
      {
        data: dataSet1,
        backgroundColor: this.getRandomColors(dataSet1.length),
        label: 'Overall Expense Samary'
      }
    ];
    this.expenseChartOptions = {
      scaleShowVerticalLines: true,
      responsive: true,
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) => {
            this.createSubChart(tooltipItem.xLabel);
            var label = data.datasets[tooltipItem.datasetIndex].label || '';

            if (label) {
              label += ': ';
            }
            label += Math.round(tooltipItem.yLabel * 100) / 100;
            return label;
          }
        }
      }
    }
  }

  createSubChart(category: string) {
    if (category && category.length > 0) {
      const trans = (this.monthFilter) ? this.filtered : this.allFinTransactions;
      const categoryFilteredTransactions = trans.filter(t => t.category == category);
      const labels = categoryFilteredTransactions.map(a => a.action);
      const data = categoryFilteredTransactions.map(a => a.cost);
      const colors = this.getRandomColors(data.length);
      this.subChartData = {
        chartType: "bar",
        labels: labels,
        data: [{
          data: data,
          backgroundColor: colors,
          label: category + ' details'
        }],
        options: {
          scales: {
            xAxes: [{
              ticks: {
                display: false
              }
            }]
          },
          legend: {
            display: false
          },
          responsive: true,
        }
      }
    } else {
      this.subChartData = undefined;
    }
  }
  addChartCard() {
    let componentFactory = this.resolver.resolveComponentFactory(ChartCardComponent);
    let componentRef = this.chartsSection.createComponent(componentFactory);
    (<ChartCardComponent>componentRef.instance).data = {
      chartType: this.chartType,
      expenseChartData: this.expenseChartData,
      expenseChartLabels: this.expenseChartLabels,
      expenseChartOptions: this.expenseChartOptions
    }
  }
}

const COLOR_OPTIONS = [
  '#7FFFD4',
  '#F5F5DC',
  '#FFE4C4',
  '#F0FFFF',
  '#000000',
  '#FFEBCD',
  '#0000FF',
  '#8A2BE2',
  '#A52A2A',
  '#DEB887',
  '#5F9EA0',
  '#7FFF00',
  '#D2691E',
  '#FF7F50',
  '#6495ED',
  '#FFF8DC',
  '#DC143C',
  '#00FFFF',
  '#00008B',
  '#008B8B',
  '#B8860B',
  '#A9A9A9',
  '#006400',
  '#BDB76B',
  '#F8F8FF',
  '#FFD700',
  '#DAA520',
  '#808080',
  '#008000',
  '#ADFF2F',
  '#F0FFF0',
  '#FF69B4',
  '#CD5C5C',
  '#4B0082',
  '#FFFFF0',
  '#F0E68C',
  '#E6E6FA',
  '#FFF0F5',
  '#7CFC00',
  '#FFFACD',
  '#ADD8E6',
  '#F08080',
  '#E0FFFF',
  '#FAFAD2',
  '#D3D3D3',
  '#90EE90',
  '#FFB6C1',
  '#FFA07A',
  '#20B2AA',
  '#87CEFA',
  '#778899',
  '#B0C4DE',
  '#FFFFE0',
  '#00FF00',
  '#32CD32',
  '#FAF0E6',
  '#FF00FF',
  '#800000',
  '#66CDAA',
  '#0000CD',
  '#BA55D3',
  '#9370D8',
  '#3CB371',
  '#7B68EE',
  '#00FA9A',
  '#48D1CC',
  '#C71585',
  '#191970',
  '#F5FFFA',
  '#FFE4E1',
  '#FFE4B5',
  '#FFDEAD',
  '#000080',
  '#FDF5E6',
  '#808000',
  '#6B8E23',
  '#FFA500',
  '#FF4500',
  '#DA70D6',
  '#EEE8AA',
  '#98FB98',
  '#AFEEEE',
  '#D87093',
  '#FFEFD5',
  '#FFDAB9',
  '#CD853F',
  '#FFC0CB',
  '#DDA0DD',
  '#B0E0E6',
  '#800080',
  '#FF0000',
  '#BC8F8F',
  '#4169E1',
  '#8B4513',
  '#FA8072',
  '#F4A460',
  '#2E8B57',
  '#FFF5EE',
  '#A0522D',
  '#C0C0C0',
  '#87CEEB',
  '#6A5ACD',
  '#708090',
  '#FFFAFA',
  '#00FF7F',
  '#4682B4',
  '#D2B48C',
  '#008080',
  '#D8BFD8',
  '#FF6347',
  '#40E0D0',
  '#EE82EE',
  '#F5DEB3',
  '#FFFFFF',
  '#F5F5F5',
  '#FFFF00',
  '#9ACD32'];