import { Component, OnInit } from '@angular/core';
import { CartService } from '../_services/cart.service';
import { Item } from '../_models/item';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private cartService: CartService) { }

  cartItems: Item[] =[];
  ngOnInit() {
   this.cartItems = this.cartService.getCartItems();
  }

}
