import { Component, OnInit } from '@angular/core';
import { RetailersService } from '../_services/retailers.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Retailer } from '../_models/retailer';
import { NewRetailerComponent } from '../new-retailer/new-retailer.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-retailer',
  templateUrl: './retailer.component.html',
  styleUrls: ['./retailer.component.css']
})
export class RetailerComponent implements OnInit {

  retailers = new Array<Retailer>();
  retailersForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private retailerService: RetailersService) { }

  ngOnInit() {
    this.loadRetailers();
    this.createRetialersForm();
  }

  addRetailer() {

    const dialogRef = this.dialog.open(NewRetailerComponent, {
      data: 'Hello world'
    });

  }

  private createRetialersForm() {
    this.retailersForm = this.formBuilder.group({
      name: ['', Validators.required],
      contactNumber: ['', Validators.required],
      emailAddress: ['', Validators.required],
      website: ['', Validators.required],
      headOfficeAddress: ['', Validators.required],
      representative: ['', Validators.required]
    });
  }

  private loadRetailers() {
    this.retailerService.retailers.subscribe(r => this.retailers.push(r));
    this.retailerService.getRetailers();
  }
}
