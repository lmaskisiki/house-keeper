import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RetailersService } from '../_services/retailers.service';

@Component({
  selector: 'app-new-retailer',
  templateUrl: './new-retailer.component.html',
  styleUrls: ['./new-retailer.component.css']
})
export class NewRetailerComponent implements OnInit {
  retailersForm: FormGroup;


  constructor(private formBuilder: FormBuilder, private retailerService: RetailersService) { }

  ngOnInit() {
    this.createRetialersForm();
  }

  createRetialersForm() {
    this.retailersForm = this.formBuilder.group({
      name: ['', Validators.required],
      contactNumber: ['', Validators.required],
      emailAddress: ['', Validators.required],
      website: ['', Validators.required],
      headOfficeAddress: ['', Validators.required],
      representative: ['', Validators.required]
    });
  }

  saveRetailer() {
    if (this.retailersForm.valid) {
      this.retailerService.addRetailer(this.retailersForm.value);
    }
  }
}
