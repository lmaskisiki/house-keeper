import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-input-box-dialog',
    template: `<div>
                    <mat-form-field>
                        <input  [(ngModel)]='name' matInput placeholder="{{data.placeholder}}">
                     </mat-form-field>
                     <button mat-button mat-raised-button style="width:100%; padding: 3px;" (click)="ngOnDestroy()" class="btn btn-primary">Done</button>
                </div>`,
})
export class InputBoxDialog implements OnDestroy {
    name: string;
    constructor(public dialogRef: MatDialogRef<InputBoxDialog>,
        @Inject(MAT_DIALOG_DATA)
        public data: DialogData) { }

    ngOnDestroy(): void {
        this.dialogRef.close(this.name);
    }
}

export interface DialogData {
    placeholder: 'Name'
}