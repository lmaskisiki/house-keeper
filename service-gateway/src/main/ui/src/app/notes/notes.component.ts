import { Component, OnInit } from '@angular/core';
import { NotesService } from '../_services/notes.service';
import { environment } from "../../environments/environment";
import { of, from, Observable } from 'rxjs';
import { delay, flatMap, map } from 'rxjs/operators';
import { pipeFromArray } from 'rxjs/internal/util/pipe';
import { MatDialog } from '@angular/material';
import { InputBoxDialog } from './input-box-dialog';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  constructor(private noteService: NotesService, public dialog: MatDialog) { }
  notes = [];
  history: { id: string, text: string, name:string }[] = [];
  note = '';
  showHistory = false;

  ngOnInit() {
  }

  addNote() {
    if (this.note.length > 2) {
      this.notes.push(this.note);
      this.note = '';
    }
  }

  clearAllNotes() {
    this.notes = [];
  }

  saveNotes() {
    const dialogRef = this.dialog.open(InputBoxDialog, {
      width: '250px',
      data: { placeholder: 'Name' }
    });

    dialogRef.afterClosed().subscribe(result => {
      const notesText = this.notes.join('#')
      result = (result) ? result : 'Unnamed - ' + new DatePipe('en-US').transform(new Date(), 'dd-MM-yy');
      this.noteService.saveNotes({ text: notesText, name: result }).subscribe(a => {
        // do nothing
      }, error => alert(error));
    });


  }

  loadHistory() {
    this.noteService.getNotes().subscribe(hist => {
      this.history = hist;
      this.showHistory = true;
    })
  }

  showHistoryItem(id: string) {
    const historyItem = this.history.find(h => h.id === id);
    if (historyItem) {
      this.showHistory = false;
      this.notes = historyItem.text.split('#');
    }
  }
}

