import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Item } from '../_models/item';
import { CartService } from '../_services/cart.service';

@Component({
  selector: 'app-item-modal',
  templateUrl: './item-modal.component.html',
  styleUrls: ['./item-modal.component.css']
})
export class ItemModalComponent implements OnInit {

   constructor(private cartService: CartService,
    public dialogRef: MatDialogRef<ItemModalComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Item) {}

  ngOnInit() {
  }


  addToCart(item:Item){
    this.cartService.addToCart(item);
  }

}
