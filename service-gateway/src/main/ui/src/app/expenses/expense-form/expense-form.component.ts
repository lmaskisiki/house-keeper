import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserProfileService } from '../../_services/user-profile.service';
import { Expense } from '../../_models/expense';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-expense-form',
  templateUrl: './expense-form.component.html',
  styleUrls: ['./expense-form.component.css']
})
export class ExpenseFormComponent implements OnInit {

  selectedExpense: Expense;
  expenseForm: FormGroup;
  expenses: Expense[] = [];

  constructor(private activatedRouter: ActivatedRoute,
    private router: Router,
    private profileService: UserProfileService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.createExpenseForm();
    this.profileService.getExpenses().subscribe(expns => this.expenses = expns);
    this.getExpenseIdFromPathParams();
  }

  get modalMessage() {
    return this.selectedExpense ? "Update Expense" : "New Expense"
  }

  saveExpense() {
    this.profileService.saveExpense(this.expenseForm.value);
    this.router.navigate(['expenses']);
  }

  updateExpense() {
    this.profileService.updateExpense(this.expenseForm.value);
    this.router.navigate(['expenses']);
  }

 private createExpenseForm() {
    this.expenseForm = this.formBuilder.group({
      id: ['', Validators.nullValidator],
      name: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
      amount: ['', Validators.required],
      startDate: new FormControl((new Date()).toISOString()),
      endDate: new FormControl((new Date()).toISOString())
    });
  }

  private getExpenseIdFromPathParams() {
    this.activatedRouter.params.subscribe(p => {
      if (p['id']) {
        const expenseId = p['id'];
        this.selectedExpense = this.expenses.find(exp => exp.id === expenseId);
        if (!this.selectedExpense) {
          this.router.navigate(['/expenses']);
        }
        this.patchFormValues();
      }
    });
  }

  private patchFormValues() {
    this.expenseForm.patchValue({
      id: this.selectedExpense.id,
      name: this.selectedExpense.name,
      description: this.selectedExpense.description,
      category: this.selectedExpense.category,
      amount: this.selectedExpense.amount,
      startDate: this.selectedExpense.startDate,
      endDate: this.selectedExpense.endDate
    });
  }
}
