import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../_services/user-profile.service';
import { Expense } from '../_models/expense';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { Transaction } from '../_models/Transaction';
import * as _ from 'lodash';
import { transition, group } from '@angular/animations';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css'],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ]
})
export class ExpensesComponent implements OnInit {
  expenses: Expense[];
  onModalView = false;
  expenseForm: FormGroup;
  markedForAction: String;
  selectedExpense: Expense;
  selectedExpenseSubject: Subject<Expense> = new Subject<Expense>();

  form: FormGroup;
  error: any;
  userId: number = 1;

  transactions: Transaction[] = [];
  groups: { [key: string]: number } = {}
  dataSource: ExampleDataSource;

  constructor(
    private profileService: UserProfileService,
    private router: Router,
    private formBuilder: FormBuilder,
    public dialog: MatDialog, ) { }

  expandedElement: any;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow')

  ngOnInit() {
    this.profileService.getExpenses().subscribe(exps => {
      this.expenses = exps;
      this.dataSource = new ExampleDataSource(this.expenses);

    });
    this.form = this.formBuilder.group({
      avatar: ['']
    });
  }

  newExpense() {
    this.router.navigate(['expenses/create']);
  }

  get tableColumns() {
    return ['description', 'amount', 'action'];
  }

  openEditWindow() {
    this.router.navigate(['expenses/update', this.markedForAction]);
  }

  saveExpense() {
    this.profileService.saveExpense(this.expenseForm.value);
  }

  updateExpense() {
    this.profileService.updateExpense(this.expenseForm.value);
  }

  markForAction = (expenseId: String) => this.markedForAction = expenseId;

  deleteExpense() {
    this.profileService.deleteExpense(this.markedForAction).subscribe(() => { });
  }

  getTotalExpenses() {
    const amounts = this.expenses.map(a => a.amount);
    return amounts.length > 0 ? amounts.reduce((sum, current) => sum + current) : 0;
  }
}

export class ExampleDataSource extends DataSource<any> {
  data: Expense[] = [];
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  constructor(data: Expense[]) {
    super();
    this.data = data;
  }
  connect(): Observable<Expense[]> {
    const rows = [];
    this.data.forEach(element => rows.push(element, { detailRow: true, element }));
    console.log(rows);
    return of(rows);
  }

  disconnect() { }
}