import { Component, OnInit, Input } from '@angular/core';
import { type } from 'os';

@Component({
  selector: 'app-pill-container',
  templateUrl: './pill-container.component.html',
  styleUrls: ['./pill-container.component.css']
})
export class PillContainerComponent implements OnInit {

  @Input() value = ''
  @Input() type='info';
  constructor() { }

  ngOnInit() {
  }

}
