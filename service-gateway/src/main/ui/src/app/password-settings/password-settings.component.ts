import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { UserProfileService } from '../_services/user-profile.service';
@Component({
  selector: 'app-password-settings',
  templateUrl: './password-settings.component.html',
  styleUrls: ['./password-settings.component.css']
})
export class PasswordSettingsComponent implements OnInit {

  passwordSettingsForm: FormGroup;
  currentUserId: String;
  constructor(private formBuilder: FormBuilder,
    private profileService: UserProfileService,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    if (!this.profileService.activeUserId) {
      throw new Error('could not find current user id');
    }

    this.currentUserId = this.profileService.activeUserId;

    this.passwordSettingsForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      passCode: ['', Validators.required]
    });

    this.passwordSettingsForm.get('confirmPassword').valueChanges.subscribe(val => {
      const password = this.passwordSettingsForm.get('password').value;
      if (password && val !== password) {
        this.passwordSettingsForm.get('confirmPassword').setErrors({ 'noMatch': true });
      } else {
        this.passwordSettingsForm.get('confirmPassword').setErrors(null);
      }
    });
  }

  updatePassword() {
    const password = this.passwordSettingsForm.get('password').value;
    this.profileService.setUserPassword(this.currentUserId, password);
  }
}
