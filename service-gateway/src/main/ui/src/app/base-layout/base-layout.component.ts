import { Component, OnInit } from '@angular/core';
import { CartService } from '../_services/cart.service';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.css']
})
export class BaseLayoutComponent implements OnInit {
  cartSize = 0;

  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.itemsSubscription.subscribe(a => this.cartSize++);
  }
}
