export class Transaction {
  constructor(public id: string, public date: Date, public action: string, public cost: number, public category: string) { }
}
