import { Vendor } from "./vendor";

export class Item {
  name: string;
  description: string;
  price: number;
  size: string;
  picture:string;
  category:string;
  vendor: Vendor;

  constructor(
    name: string,
    description: string,
    price: number,
    size: string,
    picture:string,
    category:string,
    vendor: Vendor
  ) {
    this.name = name;
    this.description = description;
    this.price = price;
    this.size = size;
    this.picture= picture,
    this.category = category;
    this.vendor = vendor;
  }
}
