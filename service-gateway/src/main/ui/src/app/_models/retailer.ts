


export class Retailer {
  public id: String;
  public name: String;
  public contactNumber: String;
  public emailAddress: String;
  public website: String;
  public logo: String;
  public representative: String;
  public headOfficeAdress: String;
  constructor(id: String, name: String) {
    this.id = id;
    this.name = name;
  }
}
