


export class UserProfile {
  public id: String;
  public firstName: String;
  public lastName: String;
  public title: String;
  public idNumber: String;
  public role: String;
  public cellNumber: String;
  public emailAddress: String;
  public password: String;
  public storeName: String;
  public companyName: String;
  public address: {}

}


