export class Vendor {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
