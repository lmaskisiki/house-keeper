


export class Expense {
  public id: String;
  constructor(
    public name: String,
    public description: String,
    public category: String,
    public amount: number,
    public startDate: Date,
    public endDate: Date
  ) { }
}
