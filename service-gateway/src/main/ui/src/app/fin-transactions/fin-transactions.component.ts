import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../_services/user-profile.service';
import { Transaction } from '../_models/Transaction';
import { MatTableDataSource } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-fin-transactions',
  templateUrl: './fin-transactions.component.html',
  styleUrls: ['./fin-transactions.component.css']
})
export class FinTransactionsComponent implements OnInit {

  transactions: Transaction[] = [];
  showUpload = false;
  groups: { [key: string]: number } = {}
  groups1 = new Map<string, number>();
  datasource = new MatTableDataSource(this.getGroupObjectKeys());
  form: FormGroup;
  constructor(
    private profileService: UserProfileService,
    private formBuilder: FormBuilder, ) { }



  ngOnInit() {
    this.getTransactions();
    this.form = this.formBuilder.group({
      avatar: ['']
    });

  }

  private getTransactions() {
    this.profileService.getFinTransactions().subscribe(group => {
      group.subscribe(val => {
        if (!this.groups[group.key])
          this.groups[group.key] = 0;
        this.groups[group.key] += val.cost;
      });
    });
  }

  getGroupObjectKeys() {
    return Object.keys(this.groups);
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('avatar').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('avatar').value);
    this.profileService.upload(formData).subscribe(
    );
  }
}