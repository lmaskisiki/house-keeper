import { Component, OnInit, Input } from "@angular/core";
import { Item } from "../_models/item";
import { ItemModalComponent } from "../item-modal/item-modal.component";
import { MatDialog } from "@angular/material";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.css"]
})
export class ItemComponent implements OnInit {
  @Input()
  item: Item;
  constructor(public dialog: MatDialog) {}

  openDialog(item: Item): void {
    const dialogRef = this.dialog.open(ItemModalComponent, {
      data: item
    });
  }
  ngOnInit() {}
}
