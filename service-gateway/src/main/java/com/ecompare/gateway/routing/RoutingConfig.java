package com.ecompare.gateway.routing;

import com.ecompare.gateway.security.models.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.handler.RoutePredicateHandlerMapping;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.resources;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class RoutingConfig {

  @Autowired
  private PasswordEncoder passwordEncoder;


  @Bean
  public RouterFunction<ServerResponse> passwordUpdate(RoutingConfig locationHandler) {
    return RouterFunctions.route(POST("/gateway/web/password/{userId}").and(accept(MediaType.APPLICATION_JSON)), locationHandler::processPassword);
  }

  private Mono<ServerResponse> processPassword(ServerRequest serverRequest) {
    var name = Mono.just(serverRequest.pathVariable("userId")).block();
    System.out.println("User id found is :" + name);
    var response = serverRequest.bodyToMono(String.class).flatMap(bodyStr -> WebClient.builder().baseUrl("http://service-client:8081").build()
      .post()
      .uri("/api/client/user-profile/update/password/" + name)
      .body(BodyInserters.fromObject(passwordEncoder.encode(bodyStr)))
      .retrieve().bodyToMono(String.class));
    return ok().body(response, String.class);
  }

  @Bean
  public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
    return builder.routes()
      .route("service-client", r -> r.path("/api/client/**")

        .uri("lb://service-client"))
      .route("user-profile", r -> r.path("/api/user-profile/**")
        .uri("lb://profile-management"))
      .route("bank-transactions", r -> r.path("/api/fin/**")
        .uri("lb://bank-transactions"))

      .build();
  }


}
