package com.ecompare.gateway.security.datasource;

import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

public interface UserDetailsDataSource {
    Mono<UserDetails> findByUsername(String username);
}
