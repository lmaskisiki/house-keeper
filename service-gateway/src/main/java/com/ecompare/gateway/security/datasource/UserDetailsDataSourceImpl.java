package com.ecompare.gateway.security.datasource;

import com.ecompare.gateway.security.models.CustomUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class UserDetailsDataSourceImpl implements UserDetailsDataSource {

    private final PasswordEncoder passwordEncoder;
    private final WebClient webClient;

    public UserDetailsDataSourceImpl(PasswordEncoder passwordEncoder, PasswordEncoder passwordEncoder1) {
        this.passwordEncoder = passwordEncoder1;
        this.webClient = WebClient.builder().baseUrl("http://service-client:8081/api/client/user-profile").build();
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return Mono.from(getUser(username));
    }

    private Mono<CustomUserDetails> getUser(String username) {
        return webClient.post().uri("/find/username")
                .body(BodyInserters.fromObject(username))
                .retrieve().bodyToMono(CustomUserDetails.class);
    }

}
