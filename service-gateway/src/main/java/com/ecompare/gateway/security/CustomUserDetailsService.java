package com.ecompare.gateway.security;

import org.springframework.security.core.userdetails.ReactiveUserDetailsService;

public interface CustomUserDetailsService extends ReactiveUserDetailsService {
}
