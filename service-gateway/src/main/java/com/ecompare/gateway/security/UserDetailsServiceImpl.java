package com.ecompare.gateway.security;

import com.ecompare.gateway.security.datasource.UserDetailsDataSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserDetailsServiceImpl implements CustomUserDetailsService {

    private final UserDetailsDataSource userDetailsSource;

    public UserDetailsServiceImpl(UserDetailsDataSource userDetailsSource) {
        this.userDetailsSource = userDetailsSource;
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return userDetailsSource.findByUsername(username);
    }
}
