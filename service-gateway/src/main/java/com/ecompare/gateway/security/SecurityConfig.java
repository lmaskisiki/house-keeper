package com.ecompare.gateway.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.authorizeExchange()
                .pathMatchers("/**").permitAll()
          .pathMatchers("/home").permitAll()
                .pathMatchers("/login").permitAll()
                .anyExchange().authenticated()
                .and()
                .httpBasic().and()
                .formLogin().and()
                .csrf().disable()
                .build();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        var encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return encoder;
    }
}
