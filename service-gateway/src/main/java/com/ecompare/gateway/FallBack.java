package com.ecompare.gateway;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Mono;

@Component
public class FallBack {

  @RequestMapping("/fallback")
  public Mono<String> fallback() {
    return Mono.just("Hello there, your serve took too long...");
  }
}
