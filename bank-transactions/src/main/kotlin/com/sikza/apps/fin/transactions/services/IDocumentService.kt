package com.sikza.apps.fin.transactions.services

 import com.sikza.apps.fin.transactions.models.BankTransaction
 import org.springframework.stereotype.Service
 import reactor.core.publisher.Flux

@Service
interface IDocumentService {
    fun writeCsv(transactions: ArrayList<BankTransaction>) : Flux<BankTransaction>
}

