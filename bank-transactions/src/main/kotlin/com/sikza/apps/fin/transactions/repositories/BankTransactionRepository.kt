
package com.sikza.apps.fin.transactions.repositories

import com.sikza.apps.fin.transactions.models.BankTransaction
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BankTransactionRepository : ReactiveMongoRepository<BankTransaction, UUID>
