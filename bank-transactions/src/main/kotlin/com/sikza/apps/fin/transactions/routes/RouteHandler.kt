package com.sikza.apps.fin.transactions.routes

import com.sikza.apps.fin.transactions.models.BankTransaction
import com.sikza.apps.fin.transactions.repositories.BankTransactionRepository
import com.sikza.apps.fin.transactions.services.DocumentService
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.codec.multipart.Part
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File
import java.io.FileWriter
import java.io.InputStream
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import java.util.stream.Stream
import kotlin.collections.ArrayList

@Component
class RouteHandler {
  @Autowired
  lateinit var documentService: DocumentService
  @Autowired
  lateinit var bankTransactionRepository: BankTransactionRepository

  var delimeter = "*|*"
  var transactionAmountPattern = "-\\d*\\.\\d*\\s"

  fun base(it: ServerRequest): Mono<ServerResponse> {
    return ServerResponse.ok().body(BodyInserters.fromObject("Welcome"))
  }

  fun uploadFile(request: ServerRequest): Mono<ServerResponse> {
    return request.body(BodyExtractors.toMultipartData()).flatMap { parts ->
      val map: Map<String, Part> = parts.toSingleValueMap()
      val filePart: FilePart = map["file"]!! as FilePart
      DataBufferUtils.join(filePart.content()).flatMap {
        ServerResponse.ok().body(BodyInserters.fromPublisher(readFile(it.asInputStream()), BankTransaction::class.java))
      }
    }
  }

  private fun readFile(inputStream: InputStream): Flux<BankTransaction> {
    var doc = PDDocument.load(inputStream)
    var documentText = PDFTextStripper().getText(doc)
    documentText = documentText.replace("\\n(-)".toRegex(), "###")
    var lines = documentText.lines()
    var transactionUnits = ArrayList<String>()

    var transactionUnitLine = ""
    for (x in 0 until lines.size - 1) {
      if (lines[x].contains("^(\\d{2}\\s[a-zA-Z]{3}\\s\\d{2})".toRegex())) {
        transactionUnitLine = lines[x]
      }
      if (x < lines.size - 2 && lines[x + 1].contains("###")) {
        transactionUnits.add(transactionUnitLine + lines[x] + lines[x + 1].replace("###", " -"))
      }
    }
    var transactionsUnitsText = transactionUnits.joinToString("\n")
    var formattedTransactionEntries = applyDelimeters(transactionsUnitsText.lines())
    var bankTransactions = findTransactionEntries(formattedTransactionEntries.joinToString(separator = "\n").trim())
    applyTransactionDateCleanup(bankTransactions)
    recordBankStatementDate(bankTransactions)
    applyTransactionCategory(bankTransactions)
    return documentService.writeCsv(bankTransactions)
  }

  private fun applyTransactionDateCleanup(bankTransactions: ArrayList<BankTransaction>) {
    var dateStore = File("date.store.txt")
    if (!dateStore.exists()) {
      return
    }
    try {
      var previousMaxTransactionDateString = dateStore.readText()
      var previousMaxTransactionDate = LocalDate.parse(previousMaxTransactionDateString)
      bankTransactions.removeIf { LocalDate.parse(it.date) <= previousMaxTransactionDate }
    } catch (ex: Exception) {
      println(ex.message)
    }
  }

  private fun recordBankStatementDate(bankTransactions: ArrayList<BankTransaction>) {
    var dateStore = File("date.store.txt")
    var maxDate = bankTransactions.maxBy { it.date }
    if (!dateStore.exists()) {
      dateStore.createNewFile()
    }
    if (maxDate != null) {
      FileWriter(dateStore).use {
        it.write(maxDate.date)
      }
    }
  }

  private fun applyTransactionCategory(bankTransactions: ArrayList<BankTransaction>) {
    bankTransactions.forEach { transaction ->
      var categoryMatch = getTransactionCategories().firstOrNull {
        it.Keys.any { key -> transaction.action.contains(key) }
      }
      if (categoryMatch != null) {
        transaction.category = categoryMatch.Value
      } else {
        transaction.category = "NO CATEGORY"
      }
    }
  }

  private fun findTransactionEntries(text: String): ArrayList<BankTransaction> {
    var bankTransactions = ArrayList<BankTransaction>()
    text.lines().forEach {
      var trans = mapToTransaction(it)
      if (trans != null) {
        bankTransactions.add(trans)
      }
    }
    return bankTransactions
  }

  private fun applyDelimeters(lines: List<String>): ArrayList<String> {
    var formattedTransactionEntries = ArrayList<String>()
    lines.forEach {
      if (it.contains("^(\\d{2}\\s[a-zA-Z]{3}\\s\\d{2})".toRegex())) {
        var completeLine = it.replace("^(\\d{2}\\s[a-zA-Z]{3}\\s\\d{2})".toRegex(), "$0 $delimeter ")
        completeLine = completeLine.replace(transactionAmountPattern.toRegex(), " $delimeter $0")
        formattedTransactionEntries.add(completeLine)
      }
    }
    return formattedTransactionEntries
  }

  fun convertToLocalDate(dateToConvert: Date): LocalDate {
    return Instant.ofEpochMilli(dateToConvert.time)
      .atZone(ZoneId.systemDefault())
      .toLocalDate()
  }

  private fun mapToTransaction(transactionLine: String): BankTransaction? {
    var parts = transactionLine.split(delimeter)
    if (parts.size == 3) {
      var fm = SimpleDateFormat("dd MMM yy")
      var date = fm.parse(parts[0].trim())
      var amount = "0"
      if (parts[2].contains("-\\d*.\\d{2}".toRegex()))
        amount = "-\\d*.\\d{2}".toRegex().find(parts[2])?.value ?: "0"
      return BankTransaction(convertToLocalDate(date).toString(), parts[1].trim(), amount.toDouble(), "")
    }
    return null
  }

  private fun getTransactionCategories(): ArrayList<TransactionCategory> {
    return arrayListOf(
      TransactionCategory(arrayListOf("Payment", "NSFAS", "Loan"), "LOAN REPAYMENTS"),
      TransactionCategory(arrayListOf("CHEQUE CARD PURCHASE"), "CARD PURCHASE"),
      TransactionCategory(arrayListOf("CASH WITHDRAWAL"), "CASH WITHDRAWAL"),
      TransactionCategory(arrayListOf("INSURANCE PREMIUM"), "INSURANCE PREMIUM"),
      TransactionCategory(arrayListOf("WITHDRAWAL FEE", "ATM FEE", "FEE"), "BANK FEES"),
      TransactionCategory(arrayListOf("INSTANT MONEY"), "INSTANT MONEY"),
      TransactionCategory(arrayListOf("MTN", "VODACOM", "CELL C", "AIRTIME", "PREPAID", "PRE-PAID", "CELLPHONE"), "DATA & MOBILE")
    )
  }
}

class TransactionCategory(
  val Keys: ArrayList<String>,
  val Value: String
)

