package com.sikza.apps.fin.transactions.services

import com.sikza.apps.fin.transactions.models.BankTransaction
import com.sikza.apps.fin.transactions.repositories.BankTransactionRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import kotlin.collections.ArrayList


@Service
class DocumentService(val bankTransactionRepository: BankTransactionRepository) : IDocumentService {

  override fun writeCsv(transactions: ArrayList<BankTransaction>): Flux<BankTransaction> {
     return Flux.fromIterable(transactions)
      .flatMap {
        bankTransactionRepository.save(it)
      }
  }
}
