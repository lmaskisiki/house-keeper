package com.sikza.apps.fin.transactions.routes

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.*
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.RequestPredicates.accept
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerResponse

@Configuration
class Routes {

  @Bean
  fun route(handler: RouteHandler): RouterFunction<ServerResponse> {
    return RouterFunctions.route()
      .POST("/api/fin/upload/", accept(MULTIPART_FORM_DATA), HandlerFunction { handler.uploadFile(it) })
      .GET("/api/fin/", accept(APPLICATION_JSON), HandlerFunction { handler.base(it) })
      .build()
  }
}
