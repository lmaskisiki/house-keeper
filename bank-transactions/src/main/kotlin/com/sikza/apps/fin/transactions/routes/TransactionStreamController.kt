package com.sikza.apps.fin.transactions.routes

import com.sikza.apps.fin.transactions.models.BankTransaction
import com.sikza.apps.fin.transactions.repositories.BankTransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

import java.time.Duration

@RestController
@RequestMapping("/api/fin/transactions")
class TransactionStreamController {

    @Autowired
    private lateinit var bankTransactionRepository: BankTransactionRepository

    @GetMapping("/", produces = ["application/json"])
    fun streamTransactions  (): Flux<BankTransaction> {
        return bankTransactionRepository!!.findAll()
//      return bankTransactionRepository!!.findAll().delayElements(Duration.ofMillis(100))

    }
}
