package com.sikza.apps.fin.transactions

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BankTransactionsApplication

fun main(args: Array<String>) {
  runApplication<BankTransactionsApplication>(*args)
}



