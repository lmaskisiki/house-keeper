package com.sikza.apps.fin.transactions.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class BankTransaction(
  val date: String,
  val action: String,
  val cost: Number,
  var category: String
) {
  @Id
  var id: UUID = UUID.randomUUID()

  override fun toString(): String {
    return "BankTransaction2(date='$date', action='$action', cost=$cost, id=$id, category='$category')"
  }
}
